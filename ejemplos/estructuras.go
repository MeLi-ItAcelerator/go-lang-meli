package main

import "fmt"

type Vertice struct {
	X int
	Y int
}

func main() {
	v := Vertice{X: 6}
	fmt.Println(v)
}
