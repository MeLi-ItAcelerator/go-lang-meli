package main

import "fmt"

func main() {
	var s [] int;
	fmt.Println(s);
	nombres := [4]string{"luis", "fabian", "juan", "raulito"};
	fmt.Println(nombres);

	//Slices se declara sin indicar el tamaño del array es dinamico

	a := nombres[0:2]
	b := nombres[1:3]
	fmt.Println(a, b)

	sl:=[]int{2,3,4,5,6,7}
	sl=sl[:0]
	imprimirSlice(sl)
	sl=sl[:4]
	imprimirSlice(sl)
	sl=sl[2:]
	imprimirSlice(sl)
	sl=sl[0:]
	sl=sl[0:0]
	imprimirSlice(sl)
c:=make([]int,0)
imprimirSlice(c);



}

func imprimirSlice (sl []int){
	fmt.Printf("len=%d cap=%d %v\n", len(sl), cap(sl),sl)
}