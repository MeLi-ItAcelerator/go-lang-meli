package domain

import "time"

//coloco en mayuscula la primer letra para que todos puedan utilizar mi puntero
type Tweet struct {
	User string
	Text string
	Date *time.Time
	Id int
}

//coloco la funcion NewTweet en mayucula para que puedan acceder e indico que devuelve un puntero a un tipo struc Tweet
func NewTweet(user string,text string) *Tweet  {
	date := time.Now()
	//creo una variable v del Tipo struck Tweet
	v := Tweet{User:user,Text:text,Date:&date}
	//devuelvo la direccion de memoria de v
	return &v
}
