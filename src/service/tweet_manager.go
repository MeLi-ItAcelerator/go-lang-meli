package service

import (
	"errors"
	"gitlab.com/vegaluis.v/MeLi-ItAcelerator/go-lang-meli/src/domain"
)

type TweetManage struct {
 mitweet *domain.Tweet
 S []*domain.Tweet
 Id int
 usersTweets map[int]*domain.Tweet
}

func NewTweetManager() TweetManage {
	 tumama := TweetManage{}
	tumama.InitializeService()
	return tumama
}

func (t *TweetManage)PublishTweet(tweet *domain.Tweet) (int,error) {
	if  tweet.Text == "" {
		return -1,errors.New("text is required")
	}else{
		if len(tweet.Text)>140 {
			return -1,errors.New("text exceeds 140 characters")
		}
	}
	t.Id++;
	tweet.Id = t.Id;
	t.mitweet=tweet
	t.S = append(t.S,t.mitweet)
	return t.Id,nil
}

func (t *TweetManage)GetTweet() *domain.Tweet {
	return t.mitweet
}

func (t *TweetManage)GetTweetById(id int) *domain.Tweet{
	return t.S[id]
}

func (t *TweetManage)GetTweets() []*domain.Tweet {
	return t.S
}

func (t *TweetManage)CountTweetsByUser(user string) int {
	cont := 0
	for _,val := range t.S  {
		if val.User == user{
			cont++
		}
	}
	return cont
}

func (t *TweetManage)GetTweetsByUser(user string) map[int]*domain.Tweet {
	cont := 0
	t.usersTweets = make(map[int]*domain.Tweet)
	for _,val := range t.S  {
		if val.User == user{

			t.usersTweets[cont]=val
			cont++
		}
	}
	return t.usersTweets
}

func (t *TweetManage)InitializeService()  {
	t.S=t.S[0:0]
	t.Id=-1
}

